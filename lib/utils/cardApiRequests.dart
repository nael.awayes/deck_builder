import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:deck_builder/Entities/Card.dart';
import 'dart:convert' as json;


class CardApiRequests {
  static var baseUrl = "https://db.ygoprodeck.com/api/v2/cardinfo.php";
  static var baseImageUrl = "https://ygoprodeck.com/pics/";
  static var baseThumbUrl = "https://ygoprodeck.com/pics_small/";

  // static Future<List<Card>> getAllCards() async {
  //   final response = await http.get(baseUrl);

  //   if (response.statusCode == 200) {
  //     var jsonResponse = json.jsonDecode(response.body);
  //   } else {
  //     throw Exception('Unable to fetch cards');
  //   }

  // }

  static Future<Card> getCardById(int id) async {
    var stringId = id.toString();

    final response = await http.get(baseUrl + "?name=" + stringId);

    if (response.statusCode == 200) {
      var jsonResponse = json.jsonDecode(response.body);
      return Card.fromJson(jsonResponse);
    } else {
      throw Exception("Unable to fetch card with Id" + stringId);
    }
  }

  static Future<http.Response> getCardImage(int cardId, bool thumb) {
    var url = thumb ? baseThumbUrl : baseImageUrl;
    var stringId = cardId.toString();
    return http.get(url + stringId + ",jpg");
  }
}
