class Card {
  final int id;
  final String name;
  final String desc;
  final String atk;
  final String def;
  final String type;
  final String image;
  final String thumb;

  Card({this.id, this.name, this.desc, this.atk, this.def, this.type, this.image, this.thumb});

  factory Card.fromJson(Map<String, dynamic> json) {
    return Card(
      id: json['id'],
      name: json['name'],
      desc: json['desc'],
      atk: json['atk'],
      def: json['def'],
      type: json['type']
    );
  }
}
